import datetime

def get_date():
    date_object = datetime.datetime.now()
    date = date_object.strftime('%d/%m/%Y')
    return str(date)

def get_time():
    time_object = datetime.datetime.now()
    time = time_object.strftime('%H.%M')
    return str(time)
