from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from kivy.core.window import Window
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.properties import ObjectProperty
from kivy.uix.label import Label
from kivy.properties import StringProperty

import os
import threading

from DateTime import get_date, get_time
from database import DataBase
from micro_control import micro_control
from firebase_storage import upload_preds, upload_origin


# Hide mouse cursor on desktop
Window.show_cursor = True

# Full Screen
Window.fullscreen = False
# Window.fullscreen = 'auto'

# Set windows size
Window.size = (800, 480)

# class library
db = DataBase()
mc = micro_control

# global
glass, can, plastic, sump = 0, 0, 0, 0
calculatescore = 0
studentID, classID, imagename, session_id = '', -1, '', -1

class PointScreen(Screen):
    total_point = StringProperty(0)
    current = ''

    def on_enter(self, *args):
        global calculatescore
        calculatescore = (db.get_point(1) * glass) + \
                        (db.get_point(2) * can) + \
                        (db.get_point(3) * plastic)
        self.total_point = str(calculatescore)

    def btn_home(self):
        global session_id, studentID

        # update_session
        if session_id != -1:
            db.update_session(session_id, self.total_point)
            session_id = -1

            # upadte total_point
            db.update_total_point(studentID, self.total_point)
            # reset
            studentID = ''

        global calculatescore
        calculatescore = 0
        global glass, plastic, can, sump
        glass, can, plastic, sump = 0, 0, 0, 0
        sm.transition.direction = "right"
        sm.current = "HomeScreen"


class ProcessScreen(Screen):
    Date = StringProperty(0)
    Time = StringProperty(0)
    User = StringProperty(None)
    glass_pie = StringProperty(0)
    plastic_pie = StringProperty(0)
    can_pie = StringProperty(0)
    sum_pie = StringProperty(0)
    current = ""

    def on_enter(self, *args):
        self.User, self.Date, self.Time, self.glass_pie, \
        self.plastic_pie, self.can_pie, self.sum_pie = self.current


class ReadyScreen(Screen):
    global glass, can, plastic, sump

    User = StringProperty(None)
    Date = StringProperty(get_date())
    Time = StringProperty(get_time())
    glass_pie = StringProperty(0)
    plastic_pie = StringProperty(0)
    can_pie = StringProperty(0)
    sum_pie = StringProperty(0)
    current = ""

    def on_enter(self, *args):
        global studentID

        if self.current == "danate":
            self.User = "User donate"
            studentID = "User donate"
        else:
            studentID = self.current
            prefix, first_name = db.get_user(self.current)
            self.User = prefix + first_name
    
    def micro_working(self):
        global glass, plastic, can, sump, imagename, studentID, session_id
    
        mc = micro_control()
        img_name, ClassFolder, imgname_origin = mc.prediction()
        upload_origin(imgname_origin, ClassFolder)
        upload_preds(img_name, ClassFolder)
        os.remove(imgname_origin)
        os.remove(img_name)

        imagename = img_name

        # update value screen
        if ClassFolder == 'glass':
            self.glass_pie = str(glass + 1)
            classID = 1
        elif ClassFolder == 'can':
            self.can_pie = str(can + 1)
            classID = 2
        else:
            self.plastic_pie = str(plastic + 1)
            classID = 3

        # if len(studentID) == 10:
        if sump == 0:
            # insert session_id
            session_id = db.insert_session()
        # insert trash
        db.insert_trash(studentID, 1, classID, session_id, imagename)
        
        self.sum_pie = str(int(self.glass_pie) + int(self.plastic_pie) + int(self.can_pie))

        # update value in global
        glass = int(self.glass_pie)
        plastic = int(self.plastic_pie)
        can = int(self.can_pie)
        sump = int(self.sum_pie)
    
        # Clock.schedule_once(self.startprocess, 0)
        self.endprocess()

    def collect(self):
        # update date/time on screen
        self.Date = get_date(); self.Time = get_time()
        self.startprocess()
        threading.Thread(target=self.micro_working).start()

    def startprocess(self):
        ProcessScreen.current = self.User, get_date(), get_time(), str(self.glass_pie), \
                                str(self.plastic_pie), str(self.can_pie), str(self.sum_pie)

        sm.transition.direction = "left"
        sm.current = "ProcessScreen"
    
    def endprocess(self):
        sm.transition.direction = "right"
        sm.current = "ReadyScreen"

    def reset(self):
        global studentID, classID, imagename
        # studentID = ''
        classID = -1
        imagename = ''

        # reset screen
        self.glass_pie = "0"
        self.plastic_pie = "0"
        self.can_pie = "0"
        self.sum_pie = "0"

    def lookscore(self):
        self.reset()
        sm.transition.direction = "left"
        sm.current = "PointScreen"


std_len = 10
class EnterIDScreen(Screen):
    sid = StringProperty()

    def one(self):
        if len(self.sid) < std_len:
            self.sid += '1'

    def two(self):
        if len(self.sid) < std_len:
            self.sid += '2'

    def three(self):
        if len(self.sid) < std_len:
            self.sid += '3'

    def four(self):
        if len(self.sid) < std_len:
            self.sid += '4'

    def five(self):
        if len(self.sid) < std_len:
            self.sid += '5'

    def six(self):
        if len(self.sid) < std_len:
            self.sid += '6'

    def seven(self):
        if len(self.sid) < std_len:
            self.sid += '7'

    def eight(self):
        if len(self.sid) < std_len:
            self.sid += '8'

    def nine(self):
        if len(self.sid) < std_len:
            self.sid += '9'

    def zero(self):
        if len(self.sid) < std_len:
            self.sid += '0'

    def delete(self):
        stdOddlen = len(self.sid)
        if stdOddlen != 0:
            self.sid = self.sid[0: stdOddlen-1]

    def ok(self):
        if db.validate(self.sid):
            ReadyScreen.current = self.sid
            sm.transition.direction = "left"
            sm.current = "ReadyScreen"
        else:
            sm.transition.direction = "left"
            sm.current = "InvalidScreen"
        self.sid = ''

    def btn_back(self):
        self.sid = ''
        sm.transition.direction = "right"
        sm.current = "LoginScreen"


class QRcodeScreen(Screen):
    def btn_back(self):
        sm.transition.direction = "right"
        sm.current = "LoginScreen"


class InvalidScreen(Screen):
    def btn_back(self):
        sm.transition.direction = "right"
        sm.current = "EnterIDScreen"


class LoginScreen(Screen):
    def btn_byQRcode(self):
        sm.transition.direction = "left"
        sm.current = "QRcodeScreen"

    def btn_byID(self):
        sm.transition.direction = "left"
        sm.current = "EnterIDScreen"

    def btn_back(self):
        sm.transition.direction = "right"
        sm.current = "ExScreen"


class HowToScreen(Screen):
    def btn_back(self):
        sm.transition.direction = "right"
        sm.current = "MenuScreen"


class ExScreen(Screen):
    def btn_collect(self):
        sm.transition.direction = "left"
        sm.current = "LoginScreen"

    def btn_donate(self):
        ReadyScreen.current = "danate"
        sm.transition.direction = "left"
        sm.current = "ReadyScreen"
        
    def btn_back(self):
        sm.transition.direction = "right"
        sm.current = "MenuScreen"


class MenuScreen(Screen):
    glass_point = StringProperty(0)
    can_point = StringProperty(0)
    plastic_point = StringProperty(0)
    
    glass_point = 'ขวดแก้ว\n' + str(db.get_point(1)) + ' คะแนน'
    can_point = 'กระป๋อง\n' + str(db.get_point(2)) + ' คะแนน'
    plastic_point = 'ขวดพลาสติก\n' +str( db.get_point(3)) + ' คะแนน'

    def btn_ex(self):
        sm.transition.direction = "left"
        sm.current = "ExScreen"

    def btn_howto(self):
        sm.transition.direction = "left"
        sm.current = "HowToScreen"


class HomeScreen(Screen):
    glass_cap = StringProperty('ขวดแก้ว 50%')
    plastic_cap = StringProperty('ขวดพลาสติก 30%')
    can_cap = StringProperty('กระป๋อง 40%')

    def btn_start(self):
        sm.transition.direction = "left"
        # sm.transition = NoTransition()
        sm.current = "MenuScreen"


class WindowManager(ScreenManager):
    pass

kv = Builder.load_file("my.kv")

sm = WindowManager()

screens = [HomeScreen(name="HomeScreen"), MenuScreen(name="MenuScreen"),
            ExScreen(name="ExScreen"), HowToScreen(name="HowToScreen"),
            LoginScreen(name="LoginScreen"), QRcodeScreen(name="QRcodeScreen"),
            EnterIDScreen(name="EnterIDScreen"), InvalidScreen(name="InvalidScreen"),
            ReadyScreen(name="ReadyScreen"), ProcessScreen(name="ProcessScreen"), 
            PointScreen(name="PointScreen")]
for screen in screens:
    sm.add_widget(screen)

sm.current = "HomeScreen"

class MyMainApp(App):
    def build(self):
        # return kv
        return sm


if __name__ == "__main__":
    MyMainApp().run()