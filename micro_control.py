import cv2
import time
import requests

class micro_control:
    def create_imgname(self):
        seconds = time.time()
        imgname = str(seconds).split('.')[0] + '.jpg'

        return imgname

    def cap_image(self):
        imgname = self.create_imgname()

        cap = cv2.VideoCapture(0)
        ret, frame = cap.read()

        imgname_origin = imgname.split('.')[0] + '-origin' + imgname.split('.')[1] + '.jpg'
        cv2.imwrite(imgname_origin, frame)

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(imgname, gray)
        cap.release()
        cv2.destroyAllWindows()

        return imgname, imgname_origin

    def prediction(self):
        img_name, imgname_origin = self.cap_image()

        url = 'http://34.126.71.148:8080/prediction'
        files = {'media': open(img_name, 'rb')}
        response = requests.post(url, files=files)

        data = response.json()

        return img_name, data['data'], imgname_origin
    
